#! /usr/bin/env python2
from subprocess import check_output

def get_user():
    return check_output("gpg -dq ~/.local/share/offlineimap/offlineimapuser.gpg", shell=True).strip("\n")

def get_pass():
    return check_output("gpg -dq ~/.local/share/offlineimap/offlineimappass.gpg", shell=True).strip("\n")
